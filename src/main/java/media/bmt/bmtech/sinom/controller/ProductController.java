package media.bmt.bmtech.sinom.controller;

import media.bmt.bmtech.sinom.model.Product;
import media.bmt.bmtech.sinom.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/api")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping(value="/products")
    public Iterable<Product> getProduct(Product product) {
        return productRepository.findAll();
    }

    @PostMapping(value="/products")
    public String addProduct(@RequestBody Product product) {
        productRepository.save(product);

        return "Product Added!";
    }

    @GetMapping(value="/products/{id}")
    public Product getOne(@PathVariable String id) {
        return productRepository.findOne(id);
    }

    @PutMapping(value="/products/{id}")
    public String update(@PathVariable String id, @RequestBody Product product) {
        Product prod = productRepository.findOne(id);
        if(product.getName() != null)
            prod.setName(product.getName());
        if(product.getPrice() != null)
            prod.setPrice(product.getPrice());
        if(prod.getProduct_desc() != null)
            prod.setProduct_desc(product.getProduct_desc());
        if(product.getQty() != 0)
            prod.setQty(product.getQty());
        productRepository.save(prod);

        return "Product Updated!";
    }

    @DeleteMapping(value="/products/{id}")
    public String delete(@PathVariable String id) {
        productRepository.delete(id);

        return "Product Deleted";
    }
}
