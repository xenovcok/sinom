package media.bmt.bmtech.sinom.controller;

import media.bmt.bmtech.sinom.data.OrderDetailsRestData;
import media.bmt.bmtech.sinom.data.OrderRestData;
import media.bmt.bmtech.sinom.model.OrderMdl;
import media.bmt.bmtech.sinom.model.Product;
import media.bmt.bmtech.sinom.repository.OrderRepository;
import media.bmt.bmtech.sinom.repository.ProductRepository;
import media.bmt.bmtech.sinom.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @GetMapping(value = "/orders")
    public Iterable<OrderMdl> getAllOrders(OrderMdl orderMdl) {
        return orderRepository.findAll();
    }

    @GetMapping(value = "/orders/{id}")
    public OrderMdl getOneOrder(@PathVariable String id) {
        return orderRepository.findOne(id);
    }

    @PostMapping(value = "/orders")
    public String addOrder(@RequestBody OrderRestData orderRestData) {
        OrderMdl omdl = new OrderMdl();
        omdl.setCreatedAt(new Date());
        omdl.setExecuteOn(orderRestData.getExecuteOn());
        omdl.setUser_details(userRepository.findOne(orderRestData.getUser_id()));
        List<Product> prod = new ArrayList<Product>();
        for(OrderDetailsRestData td:orderRestData.getProduct_details()) {
            prod.add(productRepository.findOne(td.getId()));
        }
        omdl.setProduct_details(prod);
        orderRepository.save(omdl);
        return "Order Submitted";
    }
}
