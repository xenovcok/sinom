package media.bmt.bmtech.sinom.controller;

import media.bmt.bmtech.sinom.model.User;
import media.bmt.bmtech.sinom.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/api")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping(value="/users")
    public Iterable<User> userList() {
        return userRepository.findAll();
    }

    @PostMapping(value="/users")
    public String save(@RequestBody User user) {
        userRepository.save(user);

        return user.getId();
    }

    @GetMapping(value="/users/{id}")
    public User getUserById(@PathVariable String id) {
        return userRepository.findOne(id);
    }

    @PutMapping(value="/users/{id}")
    public User update(@PathVariable String id, @RequestBody User user) {
        User usr = userRepository.findOne(id);
        if(user.getName() != null)
            usr.setName(user.getName());
        if(user.getAddress() != null)
            usr.setAddress(user.getAddress());
        if(user.getEmail() != null)
            usr.setEmail(user.getEmail());
        if(user.getPassword() != null)
            usr.setPassword(user.getPassword());
        if(user.getBorn_date() != null)
            usr.setBorn_date(user.getBorn_date());
        if(user.getRole() != null)
            usr.setRole(user.getRole());
        userRepository.save(user);
        return usr;
    }

    @DeleteMapping(value = "/users/{id}")
    public String delete(@PathVariable String id) {
        userRepository.delete(id);

        return "User Deleted!";
    }
}
