package media.bmt.bmtech.sinom.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Document(collection = "order_col")
public class OrderMdl {
    @Id
    private String id;
    private User user_details;
    private List<Product> product_details;
    private Double total;
    @NotNull
    private Date executeOn;
    private Date createdAt;
    private Date processedAt = null;

    public OrderMdl() {

    }

    public OrderMdl(String id, User user_details, List<Product> product_details, Double total, Date executeOn, Date createdAt, Date processedAt) {
        this.id = id;
        this.user_details = user_details;
        this.product_details = product_details;
        this.total = total;
        this.executeOn = executeOn;
        this.createdAt = createdAt;
        this.processedAt = processedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser_details() {
        return user_details;
    }

    public void setUser_details(User user_details) {
        this.user_details = user_details;
    }

    public List<Product> getProduct_details() {
        return product_details;
    }

    public void setProduct_details(List<Product> product_details) {
        this.product_details = product_details;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(Date executeOn) {
        this.executeOn = executeOn;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(Date processedAt) {
        this.processedAt = processedAt;
    }
}
