package media.bmt.bmtech.sinom.repository;

import media.bmt.bmtech.sinom.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    @Override
    User findOne(String id);

    @Override
    void delete(User deleted);
}
