package media.bmt.bmtech.sinom.repository;

import media.bmt.bmtech.sinom.model.OrderMdl;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<OrderMdl, String>{

}
