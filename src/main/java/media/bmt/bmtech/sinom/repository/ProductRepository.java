package media.bmt.bmtech.sinom.repository;

import media.bmt.bmtech.sinom.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {

}
