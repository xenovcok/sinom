package media.bmt.bmtech.sinom.data;

import media.bmt.bmtech.sinom.model.Product;

import java.util.Date;
import java.util.List;

public class OrderRestData {
    private String user_id;
    private List<OrderDetailsRestData> product_details;
    private Date createdAt;
    private Double total;
    private Date executeOn;
    private Date processedAt;

    public OrderRestData() {

    }

    public OrderRestData(String user_details, List<OrderDetailsRestData> product_details, Date createdAt, Double total, Date executeOn, Date processedAt) {
        this.user_id = user_details;
        this.product_details = product_details;
        this.createdAt = createdAt;
        this.total = total;
        this.executeOn = executeOn;
        this.processedAt = processedAt;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public List<OrderDetailsRestData> getProduct_details() {
        return product_details;
    }

    public void setProduct_details(List<OrderDetailsRestData> product_details) {
        this.product_details = product_details;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(Date executeOn) {
        this.executeOn = executeOn;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public void setProcessedAt(Date processedAt) {
        this.processedAt = processedAt;
    }
}
