package media.bmt.bmtech.sinom.data;

public class OrderDetailsRestData {
    private String id;
    private String name;
    private Long price;
    private int qty;
    private String product_desc;

    public OrderDetailsRestData() {

    }

    public OrderDetailsRestData(String id, String name, Long price, int qty, String product_desc) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.product_desc = product_desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(String product_desc) {
        this.product_desc = product_desc;
    }
}
