package media.bmt.bmtech.sinom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinomApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinomApplication.class, args);
	}
}
